import itertools
import os
import uuid
from datetime import datetime

import psycopg2 as pg

from pyspark import Broadcast, Accumulator
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql import functions as F
from pyspark.sql.types import StringType
from rdflib import Namespace, Literal, XSD, URIRef
from spark_etl_utils import Transform, url_encode
from spark_etl_utils.database import get_db_query_pg, get_db_table_pandas
from spark_etl_utils.rdf import generate_rdf_graph, Dataset, Site, SITE_LOCATION_ID, SiteVisit, SITE_TYPE_SITE, \
    SITE_LOCATION_VISIT_ID, SITE_TYPE_PLOT, UNIQUE_ID
from spark_etl_utils.rdf.models import Plot, FeatureOfInterest, Taxon, Observation, ObservableProperty, \
    generate_underscore_uri, Instant, Instrument
from tern_rdf.namespace_bindings import TERN
from whoosh.index import open_dir
from whoosh.qparser import QueryParser

from config import Config
from transform_tables.common import post_transform, create_apc_index


class Table(Transform):

    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = 'robson_diam_height_all_years_clean_csv'
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE
        self.matched_species = {}

        ## TODO: QUERY must select always: site_id, site_visit_id, default_datetime, foi_id (soil_char_id, ...),
        ## unique_id, sample_id (if it is a sample/material_sample...) -> read docu for more information
        query = """
          ( select 
                "site.id" as supersite_id,
                "plot.id" as plot_id,
                "plot.dimensions" as plot_dimensions,
                subplot as subplot_id,
                "subplot.dimension" as subplot_dimensions,
                "start.visit.date" as start_visit_date,
                --"end.visit.date",
                --"position.x.coordinate",
                --"position.y.coordinate",
                "plant.id" as plant_id,
                "scientific.name" as scientific_name,
                --genus,
                --"family",
                "diameter.at.breast.height" as diameter_at_breast_height,
                --"stem.diameter.unit", 
                "vegetative.height" as vegetative_height, 
                --"vegetative.height.unit", 
                "wood.density..g.cm.3." as wood_density__g_cm_3_,
                "estimated.stem.diameter.at.breast.height" as estimated_stem_diameter_at_breast_height,
                "tree.alive.status" as tree_alive_status, 
                instrument,
                "comment",
                concat("plot.id", '-subplot-', "subplot") as site_id,
                concat("plot.id") as parent_site_id,
                "start.visit.date" as site_visit_id, 
                concat("start.visit.date",'-01-01T00:00:00Z') as default_datetime,
                concat("plot.id","plant.id") as plant_ind_id,
                concat("plot.id",subplot,"start.visit.date","plant.id") as unique_id
            from supersites_diameter_height.robson_diam_height_all_years_clean_csv
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df = self.df.toDF(*(c.replace('.', '_') for c in self.df.columns))
        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df['site_visit_id'].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID))
            self.df.show()

        # Create the index for the CSV source files.
        create_apc_index()
        # Load the index.
        self.ix = open_dir(
            os.path.join(Config.APP_DIR, Config.HERBARIUM_FILES, 'index_files'))
        self.ix.writer()

    def load_lookup(self):
        dataset_version = get_db_table_pandas(
            self.db_host,
            self.db_port,
            self.db_name,
            "r_ecoplatform_metadata",
            self.db_username,
            self.db_password,
            Config.DATASET,
            query="select load_date from {}.r_ecoplatform_metadata".format(Config.DATASET),
        ).iloc[0, 0]
        ufoi_db = {
            "host": self.db_host,
            "port": self.db_port,
            "dbname": "ultimate_feature_of_interest",
            "user": self.db_username,
            "password": self.db_password,
        }
        return self.spark.sparkContext.broadcast([dataset_version, ufoi_db, self.matched_species])

    def pre_transform(self, errors):
        df_derived = self.df.replace("NA", None)
        decode_udf = udf(lambda val: url_encode(val), StringType())
        df_derived = df_derived.withColumn("supersite_id", decode_udf("supersite_id"))

        df_derived = df_derived.withColumn(
            "estimated_stem_diameter_at_breast_height",
            F.when((F.col("estimated_stem_diameter_at_breast_height") == "Yes"), True).otherwise(False))

        species_list = df_derived.select("scientific_name").distinct()
        unmatched_species = set()
        for row in species_list.rdd.collect():
            species = row["scientific_name"]
            with self.ix.searcher() as searcher:
                # (Ausplot taxon -> APC acceptedNameUsage)
                query = QueryParser("canonicalName", self.ix.schema).parse(species)
                results = searcher.search(query)
                if not results:
                    # (Ausplot taxon -> APC scientificName)
                    errors.add("[1] Not found in canonical: " + species)
                    query = QueryParser("scientificName", self.ix.schema).parse(species)
                    results = searcher.search(query)
                    if not results:
                        # (Ausplot taxon -> APC canonicalName)
                        errors.add("[2] Not found in scientific: " + species)
                        query = QueryParser("acceptedNameUsage", self.ix.schema).parse(species)
                        results = searcher.search(query)
                        if not results:
                            errors.add("[3] Not found in accepted: " + species)
                            unmatched_species.add(species)

                if results:
                    self.matched_species.update({species: dict(results[0])})

        with open(os.path.join(Config.APP_DIR, "unmatched_species-{}.txt".format(uuid.uuid4())), 'w') as fp:
            fp.write('\n'.join(unmatched_species))

        self.df = df_derived

    @staticmethod
    def transform(rows: itertools.chain, dataset: str, namespace_url: str, table_name: str,
                  vocabulary_mappings: Broadcast, vocabulary_graph: Broadcast, errors: Accumulator,
                  lookup: Broadcast = None, ontology: Broadcast = None) -> None:

        ufoi_db = lookup.value[1]
        conn = pg.connect(
            host=ufoi_db["host"],
            port=ufoi_db["port"],
            dbname=ufoi_db["dbname"],
            user=ufoi_db["user"],
            password=ufoi_db["password"],
        )

        ns = Namespace(namespace_url)

        rows, rows_cloned = itertools.tee(rows)
        g = generate_rdf_graph(
            rows,
            dataset,
            ns,
            vocabulary_mappings,
            vocabulary_graph,
            errors,
        )

        dataset_uri = Dataset.generate_uri(ns)
        g += Dataset(
            uri=dataset_uri,
            title=Literal("Supersites Diameter and Height"),
            description=Literal("""TBD"""),
            issued=Literal(lookup.value[0], datatype=XSD.date),
            creator=Literal("TBD"),
            citation=Literal("TBD"),
            publisher=Literal("TERN"),
        ).g

        # established_date = row["date_commissioned"].strftime("%Y-%m-%d")
        # start_date = datetime.strptime(row["date_commissioned"].strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")
        site_uri = Site.generate_uri(ns, url_encode("RobsonCreek"))
        # site_visit_uri = SiteVisit.generate_uri(ns, row[SITE_LOCATION_VISIT_ID])
        g += Site(
            uri=site_uri,
            identifier=Literal("Robson Creek", datatype=XSD.string),
            in_dataset=dataset_uri,
            date_commissioned=Literal(datetime.strptime("1990-05-08", "%Y-%m-%d").date(), datatype=XSD.date),
            # has_site_visit=site_visit_uri,
            location_description=Literal("description Robson creek", datatype=XSD.string),
            feature_type=URIRef(SITE_TYPE_SITE),
        ).g

        src_cursor = conn.cursor()
        plot_ids = set()
        subplot_ids = set()
        for row in rows_cloned:
            site_visit_uri = SiteVisit.generate_uri(ns, row[SITE_LOCATION_VISIT_ID])
            start_date = datetime.strptime("1990-05-08 00:00:00", "%Y-%m-%d %H:%M:%S")
            end_date = datetime.strptime("1990-05-08 00:00:00", "%Y-%m-%d %H:%M:%S")
            g += SiteVisit(
                uri=site_visit_uri,
                identifier=Literal(row["site_visit_id"], datatype=XSD.string),
                in_dataset=dataset_uri,
                has_site=site_uri,
                started_at_time=Literal(start_date, datatype=XSD.dateTime),
                ended_at_time=Literal(end_date, datatype=XSD.dateTime),
            ).g
            print(site_uri, TERN.hasSiteVisit, site_visit_uri)
            g.add((site_uri, TERN.hasSiteVisit, site_visit_uri))

            plot_uri = Plot.generate_uri(ns, row["plot_id"])
            if plot_uri not in plot_ids:
                # established_date = row["date_commissioned"].strftime("%Y-%m-%d")
                # start_date = datetime.strptime(row["date_commissioned"].strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")
                # established_date = datetime.strptime("1990-05-08", "%Y-%m-%d")
                g += Plot(
                    uri=plot_uri,
                    identifier=Literal(row["plot_id"], datatype=XSD.string),
                    in_dataset=dataset_uri,
                    is_part_of=site_uri,
                    has_site_visit=site_visit_uri,
                    feature_type=URIRef(SITE_TYPE_PLOT),
                    dimension=Literal(row["plot_dimensions"])
                ).g
                plot_ids.add(plot_uri)
            else:
                g.add((plot_uri, TERN.hasSiteVisit, site_visit_uri))
                g.add((site_visit_uri, TERN.hasSite, plot_uri))

            subplot_uri = Plot.generate_uri(ns, row[SITE_LOCATION_ID])
            if subplot_uri not in subplot_ids:
                g += Plot(
                    uri=subplot_uri,
                    identifier=Literal(row[SITE_LOCATION_ID], datatype=XSD.string),
                    in_dataset=dataset_uri,
                    is_part_of=plot_uri,
                    has_site_visit=site_visit_uri,
                    feature_type=URIRef(SITE_TYPE_PLOT),
                    dimension=Literal(row["subplot_dimensions"])
                ).g
                subplot_ids.add(subplot_uri)
            else:
                g.add((subplot_uri, TERN.hasSiteVisit, site_visit_uri))

            matched_species = lookup.value[2]
            species_name = row["scientific_name"]
            foi = FeatureOfInterest(
                uri=FeatureOfInterest.generate_uri(
                    ns,
                    "plot_id,plant_ind_id",
                    row,
                ),
                in_dataset=dataset_uri,
                feature_type=URIRef("http://linked.data.gov.au/def/tern-cv/60d7edf8-98c6-43e9-841c-e176c334d270"),
            )
            g += foi.g
            species = matched_species.get(species_name)
            if species:
                taxon = Taxon(
                    uri=URIRef(species["taxonID"]),
                    label=Literal(species["acceptedNameUsage"]),
                    accepted_name_usage=Literal(species["acceptedNameUsage"]),
                    accepted_name_usage_id=URIRef(species["acceptedNameUsageID"]),
                    _class=Literal(species["class_"]),
                    family=Literal(species["family"]),
                    higher_classification=Literal(species["higherClassification"]),
                    kingdom=Literal(species["kingdom"]),
                    name_according_to=Literal(species["nameAccordingTo"]),
                    name_according_to_id=URIRef(species["nameAccordingToID"]),
                    nomenclatural_code=Literal(species["nomenclaturalCode"]),
                    nomenclatural_status=Literal(species["nomenclaturalStatus"]),
                    scientific_name=Literal(species["scientificName"]),
                    scientific_name_id=URIRef(species["scientificNameID"]),
                    scientific_name_authorship=Literal(species["scientificNameAuthorship"]),
                    taxon_concept_id=URIRef(species["taxonConceptID"]),
                    taxon_id=URIRef(species["taxonID"]),
                    taxonomic_status=Literal(species["taxonomicStatus"]),
                )
                subplot_uri = Site.generate_uri(ns, row[SITE_LOCATION_ID])
                # site_visit_uri = SiteVisit.generate_uri(ns, row[SITE_LOCATION_VISIT_ID])
                g += Observation(
                    uri=Observation.generate_uri(
                        ns,
                        "robson_diam_height_all_years_clean_csv",
                        "acceptedName",
                        row[UNIQUE_ID],
                    ),
                    feature_of_interest=foi,
                    in_dataset=dataset_uri,
                    observed_property=ObservableProperty(
                        uri=generate_underscore_uri(ns),
                        value=URIRef("http://linked.data.gov.au/def/tern-cv/70646576-6dc7-4bc5-a9d8-c4c366850df0"),
                        vocabulary=URIRef("http://linked.data.gov.au/def/tern-cv/5699eca7-9ef0-47a6-bcfb-9306e0e2b85e"),
                        local_value=None,
                        local_vocabulary=None,
                    ),
                    used_procedure=URIRef(
                        "http://linked.data.gov.au/def/ausplots-cv/1d39b897-d6d3-40e9-8113-8ebeab2cd38e"),
                    has_result=taxon,
                    has_simple_result=Literal(species_name, datatype=XSD.string),
                    phenomenon_time=Instant(
                        uri=generate_underscore_uri(ns),
                        datetime=Literal(row["default_datetime"], datatype=XSD.dateTime)
                    ),
                    result_time=Literal(row["default_datetime"], datatype=XSD.dateTime),
                    used_instrument=None,
                    has_site=subplot_uri,
                    has_site_visit=site_visit_uri
                ).g

            if row["instrument"]:
                instr_uri_1 = Instrument.generate_uri(ns,
                                                      "robson_diam_height_all_years_clean_csv",
                                                      "vegetative_height",
                                                      row[UNIQUE_ID],
                                                      False,
                                                      )
                instr_1 = Instrument(
                    uri=instr_uri_1,
                    instrument_type=URIRef(
                        "http://linked.data.gov.au/def/tern-cv/40261295-d985-4739-a420-048093e4d3ac"),
                )
                g += instr_1.g
                obs_uri_1 = Observation.generate_uri(
                    ns,
                    "robson_diam_height_all_years_clean_csv",
                    "vegetative_height",
                    row[UNIQUE_ID],
                )
                g.add((obs_uri_1, TERN.usedInstrument, instr_uri_1))
                instr_uri_2 = Instrument.generate_uri(ns,
                                                      "robson_diam_height_all_years_clean_csv",
                                                      "diameter_at_breast_height",
                                                      row[UNIQUE_ID],
                                                      False,
                                                      )
                instr_2 = Instrument(
                    uri=instr_uri_2,
                    instrument_type=URIRef(
                        "http://linked.data.gov.au/def/tern-cv/40261295-d985-4739-a420-048093e4d3ac"),
                )
                g += instr_2.g
                obs_uri_2 = Observation.generate_uri(
                    ns,
                    "robson_diam_height_all_years_clean_csv",
                    "diameter_at_breast_height",
                    row[UNIQUE_ID],
                )
                g.add((obs_uri_2, TERN.usedInstrument, instr_uri_2))

        post_transform(g, ontology.value, table_name)
