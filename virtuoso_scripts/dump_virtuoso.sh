#!/bin/sh

if [ $# -eq 1 ]
then
    podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=dump_one_graph('http://$1', './dumps/$1_', 10000000000);"
    echo "DUMP FINISHED" > /data/virtuoso/dumps/DUMP_FINISHED
else
    echo "Invalid number of arguments! Usage:"
    echo "sh dump_virtuoso.sh DATASET GRAPH_NAME"
fi
