#!/bin/sh

if [ $# -eq 2 ]
then
  #podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=log_enable(3)" \
  #&& podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=UPDATE DB.DBA.RDF_QUAD TABLE OPTION (index RDF_QUAD_GS) SET g = iri_to_id ('http://$2_$3')  WHERE g = iri_to_id ('http://$2', 0)" \
  #&& podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=log_enable(1)" \
  ##&& podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=SPARQL CLEAR GRAPH <http://$_vocabs>" \
  ##&& podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=ld_dir_all('files/vocabs', '%.rdf', 'http://$2_vocabs')" \
  podman restart virtuoso \
  && sleep 60 \
	&& podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=delete from DB.DBA.load_list" \
	&& podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=ld_dir_all('$1', '%.gz', 'http://$2')" \
	&& podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v "EXEC=rdf_loader_run()" \
	&& podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v "EXEC=checkpoint" \
  && podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=SELECT count(*) as num_errors FROM DB.DBA.load_list WHERE ll_error IS NOT NULL" \
  && podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=select * from DB.DBA.load_list WHERE ll_error IS NOT NULL" \
  && echo "finished" > FINISHED \
  && podman restart virtuoso \
  && sleep 10 \
  && rm -f /data/virtuoso/files/gzip/*
  ##&& rm -f /data/virtuoso/files/vocabs/*

else
    echo "Invalid number of arguments! Usage:"
    echo "sh load_data_virtuoso.sh LOAD_REMOTE_DIR DESTINATION_GRAPH_NAME"
fi
