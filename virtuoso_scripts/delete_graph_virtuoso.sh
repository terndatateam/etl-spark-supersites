#!/bin/sh

if [ $# -eq 1 ]
then
  podman restart virtuoso \
  && sleep 60 \
  && podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=log_enable(3,1)" \
  && podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=DELETE FROM rdf_quad WHERE g = iri_to_id ('http://$1');" \
  && podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v "EXEC=checkpoint" \
  && echo "finished" > DELETE_FINISHED \
  && podman restart virtuoso \
  && sleep 20

else
    echo "Invalid number of arguments! Usage:"
    echo "sh delete_graph_virtuoso.sh DELETE_GRAPH_NAME"
fi
