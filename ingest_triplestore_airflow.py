import argparse
import datetime
import gzip
import os
import shutil
import tarfile
import time
from zipfile import ZipFile, ZIP_DEFLATED

import requests
from SPARQLWrapper import SPARQLWrapper
from jumpssh import SSHSession
from requests.auth import HTTPBasicAuth
from scp import SCPClient

from config import Config

TRIPLESTORE_PROXY_GATE = "bastion.tern.org.au"
TRIPLESTORE_SERVER_USER = "ec2-user"
# In dev environment, use your own SSH Key
SSH_KEY = '/home/ec2-user/.ssh/tern_key'

VIRTUOSO_DATA_FOLDER = "/data/virtuoso"
ANZOGRAPH_DATA_FOLDER = "/home/ec2-user/azg-data"
GRAPHDB_DATA_FOLDER = "/graphdb-import"
LOAD_DATA_VIRTUOSO_SH = "virtuoso_scripts/load_data_virtuoso.sh"
DELETE_GRAPH_VIRTUOSO_SH = "virtuoso_scripts/delete_graph_virtuoso.sh"
DUMP_DATA_VIRTUOSO_SH = "virtuoso_scripts/dump_virtuoso.sh"


def ingest_virtuoso(triplestore_ip):
    path = os.path.join(Config.APP_DIR, Config.OUTPUT_DIR)
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if file[-4:] == '.ttl':
                files.append(os.path.join(r, file))

    print('Files:', files)
    print('Number of files:', len(files))

    historical_dir = path + time.strftime("/%Y%m%d")
    errors_dir = historical_dir + '/errors'
    if not os.path.exists(historical_dir):
        os.makedirs(historical_dir)
    if not os.path.exists(errors_dir):
        os.makedirs(errors_dir)

    start_time = time.time()

    gateway_session = SSHSession(TRIPLESTORE_PROXY_GATE, TRIPLESTORE_SERVER_USER, private_key_file=SSH_KEY).open()
    remote_session = gateway_session.get_remote_session(triplestore_ip, private_key_file=SSH_KEY)
    scp = SCPClient(remote_session.ssh_transport)
    scp.put(Config.APP_DIR + '/' + LOAD_DATA_VIRTUOSO_SH, remote_path=VIRTUOSO_DATA_FOLDER)
    scp.put(Config.APP_DIR + '/' + DUMP_DATA_VIRTUOSO_SH, remote_path=VIRTUOSO_DATA_FOLDER)
    scp.put(Config.APP_DIR + '/' + DELETE_GRAPH_VIRTUOSO_SH, remote_path=VIRTUOSO_DATA_FOLDER)

    print("Uploading Turtle files one by one to virtuoso server (SCP)")

    for f in files:
        gzip_filename = f + '.gz'
        with open(f, 'rb') as f_in:
            with gzip.open(gzip_filename, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

        try:
            scp.put(gzip_filename, remote_path=VIRTUOSO_DATA_FOLDER + "/files/gzip/")
            os.rename(gzip_filename, historical_dir + '/' + os.path.basename(gzip_filename))

        except Exception as err:
            print('Error uploading file to Virtuoso server: ', err)
            os.rename(gzip_filename, errors_dir + '/' + os.path.basename(gzip_filename))

    print('Files uploaded in {:.2f} seconds'.format(time.time() - start_time))


def cli():
    parser = argparse.ArgumentParser(description='Ingest RDF data into triplestores')
    subparsers = parser.add_subparsers(help='Triplestore')

    # Virtuoso
    virtuoso_parser = subparsers.add_parser('virtuoso', help='Ingest into Virtuoso open source')
    virtuoso_parser.add_argument('--triplestore', action="store", default="virtuoso", help=argparse.SUPPRESS)
    virtuoso_parser.add_argument('--ip', action="store", dest='ip', required=True, help='Virtuoso private IP')

    # Anzograph
    anzo_parser = subparsers.add_parser('anzograph', help='Ingest into Anzograph')
    anzo_parser.add_argument('--triplestore', action="store", default="anzograph", help=argparse.SUPPRESS)
    anzo_parser.add_argument('--ip', action="store", dest='ip', required=True, help='Anzograph private IP')
    anzo_parser.add_argument('--sparql_endpoint', action="store", dest='sparql_endpoint', required=True,
                             help='Anzograph SPARQL endpoint')
    anzo_parser.add_argument('--user', action="store", dest='user', required=True, help='Anzograph user')
    anzo_parser.add_argument('--password', action="store", dest='password', required=True, help='Anzograph password')
    anzo_parser.add_argument('--dataset', action="store", dest='dataset', required=True, help='Dataset name')

    # GraphDB
    graphdb_parser = subparsers.add_parser('graphdb', help='Ingest into GraphDB')
    graphdb_parser.add_argument('--triplestore', action="store", default="graphdb", help=argparse.SUPPRESS)
    graphdb_parser.add_argument('--ip', action="store", dest='ip', required=True, help='GraphDB private IP')
    graphdb_parser.add_argument('--repository', action="store", dest='repository', required=True,
                                help='GraphDB repository')
    graphdb_parser.add_argument('--user', action="store", dest='user', required=True, help='GraphDB user')
    graphdb_parser.add_argument('--password', action="store", dest='password', required=True, help='GraphDB password')
    graphdb_parser.add_argument('--dataset', action="store", dest='dataset', required=True, help='Dataset name')

    return parser.parse_args()


def ingest_graphdb(triplestore_ip, repository, user, password, dataset):
    path = os.path.join(Config.APP_DIR, Config.OUTPUT_DIR)
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if file[-4:] == '.ttl':
                files.append(os.path.join(r, file))

    print('Files:', files)
    print('Number of files:', len(files))

    loading_date = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
    historical_dir = path + "/" + loading_date
    if not os.path.exists(historical_dir):
        os.makedirs(historical_dir)

    start_time = time.time()

    zip_filename = '{dataset}_{time}.ttl.zip'.format(dataset=dataset, time=loading_date)
    with ZipFile(zip_filename, 'w', compression=ZIP_DEFLATED) as zip:
        # writing each file one by one
        for file in files:
            zip.write(file)
    os.rename(zip_filename, historical_dir + '/' + os.path.basename(zip_filename))

    gateway_session = SSHSession(host=TRIPLESTORE_PROXY_GATE, username=TRIPLESTORE_SERVER_USER,
                                 private_key_file=SSH_KEY).open()
    remote_session = gateway_session.get_remote_session(host=triplestore_ip, private_key_file=SSH_KEY)
    scp = SCPClient(remote_session.ssh_transport)
    scp.put(historical_dir + '/' + zip_filename, remote_path=GRAPHDB_DATA_FOLDER)

    print('Files uploaded in {:.2f} seconds'.format(time.time() - start_time))

    context = 'http://{dataset}-{time}'.format(dataset=dataset, time=loading_date)
    import_body = {
        'fileNames': [zip_filename],
        'importSettings': {
            'context': context,
            'baseURI': 'http://linked.data.gov.au/dataset/{}/'.format(dataset)
        }
    }

    requests.post(
        "https://graphdb.tern.org.au/rest/data/import/server/{}".format(repository),
        headers={"Content-Type": "application/json",
                 "Cache-Control": "no-cache"},
        auth=HTTPBasicAuth(
            user,
            password,
        ),
        json=import_body,
    )

    start_time2 = time.time()
    while True:
        files_response = requests.get(
            "https://graphdb.tern.org.au/rest/data/import/server/{}".format(repository),
            headers={"Accept": "application/json",
                     "Cache-Control": "no-cache"},
            auth=HTTPBasicAuth(
                user,
                password,
            )).json()
        file_check = list(
            filter(lambda file: file.get("name") == zip_filename and file.get("status") == "DONE", files_response))
        if file_check:
            print('Files imported in {:.2f} seconds'.format(time.time() - start_time2))
            break;
        print("Importing data...")
        time.sleep(30)

    ssh_cmd = "rm {graphdb_dir}/{file}".format(graphdb_dir=GRAPHDB_DATA_FOLDER, file=zip_filename)
    remote_session.ssh_client.exec_command(ssh_cmd)

    named_graphs = requests.get(
        "https://graphdb.tern.org.au/repositories/{}/contexts".format(repository),
        headers={"Accept": "application/sparql-results+json"},
        auth=HTTPBasicAuth(
            user,
            password,
        ),
    ).json()["results"]["bindings"]
    print(named_graphs)

    for graph in named_graphs:
        name = graph.get("contextID").get("value")
        if "http://{}".format(dataset) in name and name != context:
            print(requests.post(
                "https://graphdb.tern.org.au/repositories/{}/statements".format(repository),
                headers={"Accept": "application/json"},
                params={"update": "CLEAR GRAPH <{}>".format(name)},
                auth=HTTPBasicAuth(
                    user,
                    password,
                )).status_code)

    # map(lambda graph: print(requests.post(
    #     "https://graphdb.tern.org.au/repositories/{}/statements".format(repository),
    #     headers={"Accept": "application/json"},
    #     params={"update": "CLEAR GRAPH <{}>".format(graph.get("contextID").get("value"))},
    #     auth=HTTPBasicAuth(
    #         user,
    #         password,
    #     )).status_code), filter(
    #     lambda graph: dataset in graph.get("contextID").get("value") and graph.get("contextID").get("value") != context,
    #     named_graphs))

    print('Data loaded into GraphDB in {:.2f} seconds'.format(time.time() - start_time))


def ingest_anzograph(triplestore_ip, sparql_endpoint, user, password, dataset):
    LOAD_DATA_SPARQL_QUERY = "LOAD with 'global' <dir:/azg-data/**REPLACE_FILENAME**> INTO GRAPH <http://**REPLACE_DATASET**>"
    CLEAR_GRAPH_SPARQL_QUERY = "CLEAR GRAPH <http://**REPLACE_DATASET**>"

    path = os.path.join(Config.APP_DIR, Config.OUTPUT_DIR)
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if file[-4:] == '.ttl':
                files.append(os.path.join(r, file))

    print('Files:', files)
    print('Number of files:', len(files))

    loading_date = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
    historical_dir = path + "/" + loading_date
    if not os.path.exists(historical_dir):
        os.makedirs(historical_dir)

    start_time = time.time()

    for f in files:
        gzip_filename = f + '.gz'
        with open(f, 'rb') as f_in:
            with gzip.open(gzip_filename, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

        os.rename(gzip_filename, historical_dir + '/' + os.path.basename(gzip_filename))

    tar_filename = '{dataset}_{time}.ttl.gz.tar'.format(dataset=dataset, time=loading_date)
    with tarfile.open(historical_dir + '/' + tar_filename, mode='x:') as archive:
        archive.add(historical_dir, arcname='')
        gateway_session = SSHSession(host=TRIPLESTORE_PROXY_GATE, username=TRIPLESTORE_SERVER_USER,
                                     private_key_file=SSH_KEY).open()
        remote_session = gateway_session.get_remote_session(host=triplestore_ip, private_key_file=SSH_KEY)
        scp = SCPClient(remote_session.ssh_transport)
    scp.put(historical_dir + '/' + tar_filename, remote_path=ANZOGRAPH_DATA_FOLDER)

    print('Files uploaded in {:.2f} seconds'.format(time.time() - start_time))

    start_time = time.time()
    sparql = SPARQLWrapper(sparql_endpoint)
    sparql.setCredentials(user, password)
    sparql.method = 'POST'
    sparql.setTimeout(30000)

    sparql.setQuery(CLEAR_GRAPH_SPARQL_QUERY.replace("**REPLACE_DATASET**", dataset))
    sparql.query().convert()

    sparql.setQuery(
        LOAD_DATA_SPARQL_QUERY.replace("**REPLACE_DATASET**", dataset).replace("**REPLACE_FILENAME**", tar_filename))
    response = sparql.query().convert()

    ssh_cmd = "mv {anzo_dir}/{file} {anzo_dir}/previous_ingestions/{file}".format(anzo_dir=ANZOGRAPH_DATA_FOLDER,
                                                                                  file=tar_filename)
    remote_session.ssh_client.exec_command(ssh_cmd)

    print(response)
    print('Data loaded into Anzograph in {:.2f} seconds'.format(time.time() - start_time))


def main(args):
    if args.triplestore == "anzograph":
        ingest_anzograph(args.ip, args.sparql_endpoint, args.user, args.password, args.dataset)
    elif args.triplestore == "virtuoso":
        ingest_virtuoso(args.ip)
    elif args.triplestore == "graphdb":
        ingest_graphdb(args.ip, args.repository, args.user, args.password, args.dataset)


if __name__ == '__main__':
    args = cli()
    main(args)
