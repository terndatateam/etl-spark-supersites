import codecs
import os
import time
import traceback

from pyspark import SparkContext
from pyspark.sql import SparkSession
from spark_etl_utils import parse_vocab_from_url, ListAccumulator
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf.vocabulary_mappings import upload_vocabulary_mappings
from tern_rdf import TernRdf, DIAM_AND_HEIGHT_BINDINGS


from config import Config
from transform_tables.common import perform_transform, load_ontology, create_taxonomy_index


def match_taxanomy_api(spark):
    db_host = os.getenv('POSTGRES_HOST')
    db_port = os.getenv('POSTGRES_PORT')
    db_name = os.getenv('POSTGRES_DB')
    db_username = os.getenv('POSTGRES_USER')
    db_password = os.getenv('POSTGRES_PASSWORD')
    query = """
          ( select distinct 
                robson."scientific.name" as scientificName
            from supersites_diameter_height.robson_diam_height_all_years_clean_csv robson 
          ) q
        """

    df = get_db_query_pg(
        spark,
        db_host,
        db_port,
        db_name,
        query,
        db_username,
        db_password,
    )

    df.toPandas().to_csv("temp.csv", sep=',', header=True, index=False)
    import requests

    url = "https://api-dev.tern.org.au/taxon/v1/match.csv"

    # payload = "scientificName,kingdom\r\nHedypnois rhagadioloides,Plantae\r\nCressa cretica,Plantae\r\nRhodanthe chlorocephaia var rosea,Plantae"
    headers = {
        'Content-Type': 'text/csv'
    }
    with open("temp.csv", "r") as f:
        response = requests.request("POST", url, headers=headers, data=f.read(), verify=False)
        # with open(f"{Config.HERBARIUM_FILES}/output_species.csv", "w") as f:
        with codecs.open(f"{Config.HERBARIUM_FILES}/output_species.csv", "w", encoding = 'utf8') as f2:
            f2.write(response.text)

    create_taxonomy_index()


if __name__ == '__main__':
    start_time = time.time()

    sc = SparkContext()
    spark = SparkSession.builder \
        .master(Config.APP_MASTER) \
        .appName(Config.APP_NAME) \
        .getOrCreate()

    ## TODO: do we want to match with API and APNI?
    # match_taxanomy_api(spark)

    TABLES = Config.tables

    if Config.RELOAD_MAPPINGS:
        upload_vocabulary_mappings(spark, Config.MAPPING_CSV, Config.DATASET, "r_vocabulary_mappings", mode="overwrite")
        upload_vocabulary_mappings(spark, Config.MAPPING_YB_CSV, Config.DATASET, "r_yb_vocabulary_mappings",
                                   mode="overwrite")




    # Get and broadcast the Controlled Vocabularies for the dataset
    vocabs_g = spark.sparkContext.broadcast(
        parse_vocab_from_url(parse_vocab_from_url(TernRdf.Graph([DIAM_AND_HEIGHT_BINDINGS]), Config.VOCABS[0]), Config.VOCABS[1]))

    ontology_graph = load_ontology(Config.ONTOLOGIES)
    ontology = spark.sparkContext.broadcast(ontology_graph)

    FAILURES = []

    for table_name in TABLES:
        errors = spark.sparkContext.accumulator([], ListAccumulator())

        print('-- {} ----------------------------------------------------------------------------------'.format(
            table_name))
        table_start_time = time.time()

        try:
            mod = __import__('transform_tables.{}'.format(table_name), fromlist=['Table'])
            table = mod.Table(spark)
            table_transform = table.transform
            # table.clean()
            # table.validate()
            table.pre_transform(errors)
            vocab_mappings = table.load_vocabulary_mappings()
            lookup = table.load_lookup()

            if hasattr(table, 'perform_transform'):
                if not table.perform_transform:
                    print('{} transform job finished in {:.2f} seconds'.format(table_name,
                                                                               time.time() - table_start_time))
                    print('-------------\n')
                    continue

            perform_transform(table, table_transform, table.dataset, table.namespace, table_name, vocab_mappings, vocabs_g,
                              errors, lookup, ontology)
            if len(errors.value) > 0:
                FAILURES.append(table_name)
        except Exception as e:
            traceback.print_exception(type(e), e, e.__traceback__)
            FAILURES.append(table_name)

        print('{} transform job finished in {:.2f} seconds'.format(table_name, time.time() - table_start_time))
        print('-------------\n')

    print('\n-- Output ----------------------------------------------------------------------------------')
    print('Total Spark job finished in {:.2f} seconds'.format(time.time() - start_time))
    if FAILURES:
        with open(os.path.join(Config.APP_DIR, "SUCCESS"), 'w') as fp:
            pass
        print('Failed tables: {}'.format(FAILURES))
    else:
        # Creating a file at specified location
        with open(os.path.join(Config.APP_DIR, "SUCCESS"), 'w') as fp:
            pass
        print('No failures.')
